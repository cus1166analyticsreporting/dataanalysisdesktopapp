### Data Analysis and Reporting Platform Project (CUS 1166 Spring 2018) ###

The Data Analysis and Reporting Platform should organize all data recorded on the cloud so
they can be easily searchable and accessible by a data analysis.
Moreover, the platform will allow the Data scientist to execute predefined data analysis scripts
of selected data. Finally, the platform should visualize the analysis results in a form for a simple
graph.

### Pull Requests & Merges ###

Please create a pull request each time you want to make a change to master!
Before you merge or issue a pull request, please merge the latest updates with your own branch and 
make sure everything is working before you issue a pull request.

### Setup ###

	# Clone this repository
	git clone https://bitbucket.org/cus1166analyticsreporting/dataanalysisdesktopapp
	# Go into the repository
	cd dataanalysisdesktopapp
	
	# Install dependencies
	npm install
	# Run the app
	npm start
	
	# Start tracking with git
	git init
	git remote add origin https://bitbucket.org/cus1166analyticsreporting/dataanalysisdesktopapp
	
	# Pull new changes
	git pull origin master
	
	# Create a new branch
	git checkout -b myFeature
	
	# Commit your changes on your local branch
	git commit -m "describe your changes here"
	
	# Update the cloud branch with the changes on your local branch
	git push origin myFeature
	
	# Update your branch with the changes on master
	git checkout yourBranchName 
	git merge master
	
	# Update your branch with the changes on another branch
	git fetch
	git checkout yourBranchName 
	git merge otherBranchName
	
	# If you have a merge conflict, open atom, fix it, save it, and run:
	git add .
	git commit -m "merged branches"