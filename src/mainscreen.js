import React from 'react';
import ReactDOM from 'react-dom';

class MainScreen extends React.Component {
  render(){
    return (
      // <div className="mainscreen-wrapper">
        <div className="mainscreen">
          {this.props.children}
        </div>
        
      // </div>
    )
  }
}

export default MainScreen;
