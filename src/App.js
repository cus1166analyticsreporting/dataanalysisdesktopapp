import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import Container from './Container'
import dataAnalysisApp from './redux/reducers'

class App extends Component{
  render(){
    return (
      <Container/>
    )
  }
}

const store = createStore(dataAnalysisApp)

ReactDOM.render(
  <Provider store={store}>
    <App/>
  </Provider>,
  document.getElementById('root')
);

export default store;