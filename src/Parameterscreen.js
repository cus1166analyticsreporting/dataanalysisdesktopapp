import React from 'react';
import ReactDOM from 'react-dom';

class ParameterScreen extends React.Component {
  render(){
    return (
      // <div className="mainscreen-wrapper">
        <div className="parameterscreen">
          {this.props.children}
        </div>

      // </div>
    )
  }
}

export default ParameterScreen;
