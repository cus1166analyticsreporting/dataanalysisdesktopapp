
var React = require('react');
var ReactDOM = require('react-dom');
var ReactBsTable = require('react-bootstrap-table');
var BootstrapTable = ReactBsTable.BootstrapTable;
var TableHeaderColumn = ReactBsTable.TableHeaderColumn;

const selectRowProp = {
  mode: "checkbox",
  clickToSelect: true,
  bgColor: 'gold'
};

var subjects = [{
  researchid: 1,
    id: 1,
    name: "Andrea",
    description: "Female"
}, {
    researchid: 1,
    id: 2,
    name: "John",
    description: "Male"
  }, {
    researchid: 1,
    id: 3,
    name: "Josh",
    description: "Male"
  }, {
    researchid: 1,
    id: 4,
    name: "Jose",
    description: "Male"
  }, {
    researchid: 2,
    id: 1,
    name: "Nick",
    description: "Male"
}, {
    researchid: 2,
    id: 2,
    name: "Chris",
    description: "Male"
  }, {
    researchid: 2,
    id: 3,
    name: "Maria",
    description: "Female"
  }, {
    researchid: 2,
    id: 4,
    name: "Tina",
    description: "Female"
  }, {
    researchid: 3,
    id: 1, 
    name: "Jasmine",
    description: "Female"
  },{
    researchid: 3,
    id: 2,
    name: "Kaylin",
    description: "Female"
  },{
    researchid: 3,
    id: 3,
    name: "Sarah",
    description: "Female"
  },{
    researchid: 3,
    id: 4,
    name: "Joseph",
    description: "Man"
}]
class MultiSelectTable extends React.Component {
  render() 
  {
    const options = {
      searchPosition: 'left',
    };
    return (
      <div className = "mainmenu" >
        <BootstrapTable data={subjects} selectRow={selectRowProp} options={options}search>
          <TableHeaderColumn dataField='researchid'  width='400' >Research ID</TableHeaderColumn>
          <TableHeaderColumn dataField='id' isKey width = '100' > ID</TableHeaderColumn>
          <TableHeaderColumn dataField='name' width = '200'> Name</TableHeaderColumn>
          <TableHeaderColumn dataField='description' width = '300' > Description</TableHeaderColumn>
        </BootstrapTable>
      </div>
    );
  }
}
export default MultiSelectTable