import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom';
import ParameterMenu from "./parametermenu";
import MainMenu from './mainmenu';
var React = require('react');
var ReactDOM = require('react-dom');
var ReactBsTable = require('react-bootstrap-table');
var BootstrapTable = ReactBsTable.BootstrapTable;
var TableHeaderColumn = ReactBsTable.TableHeaderColumn;

var data = [{
    researchid: 1,
    research: "Philly",
    description: "Adults"
}, {
    researchid: 2,
    research: "New York",
    description: "Teens"
}, {
    researchid: 3, 
    research: "St. Johns University",
    description: "psychos"
}]
class SelectTable extends React.Component {
  render() {
    const selectRowProp = {
      mode: "radio",
      clickToSelect: true,
      hideSelectColumn: true,
      bgColor: 'gold',
    };
    const options = {
      searchPosition: 'left',
      onRowDoubleClick: function (row) {

      }
    }

  
    return (
      
        <div className = "mainmenu" > 
          <BootstrapTable data={data} selectRow={selectRowProp} options={options}search>
            <TableHeaderColumn dataField='researchid' isKey width = '100' > ID</TableHeaderColumn>
            <TableHeaderColumn dataField='research' width = '200'> Research</TableHeaderColumn>
            <TableHeaderColumn dataField='description' width = '300' > Description</TableHeaderColumn>
        </BootstrapTable>
        </div>
      
    )
  }
}
export default SelectTable