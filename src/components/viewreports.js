import React from 'react';
import ReactDOM from 'react-dom';
import VideoPlayer from './videoplayer';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom';


class ViewReports extends React.Component {
  constructor(props) {
    super(props);
  }
  render(){
    return (
      <Router>
        <div className="mainscreen">
          <Switch>
            <div>
              <Route component={VideoPlayer} />
            </div>
          </Switch>
        </div>
      </Router>
    )
  }
}
  
export default ViewReports;
