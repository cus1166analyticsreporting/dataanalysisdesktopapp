import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import MultiSelectTable from './multiselect';
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom';




class ParameterMenu extends React.Component {
  constructor(){
    super();
    
  }

  render(){
    return (
      <Router>
        <div className="parametermenu">
          <Switch>
            <div>
              <Route component={MultiSelectTable} />
            </div>
          </Switch>
        </div>
      </Router>
    )
  }
}

export default ParameterMenu;
