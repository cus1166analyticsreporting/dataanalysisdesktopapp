import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  Redirect
} from 'react-router-dom';
import ParameterMenu from './parametermenu';
import ParameterScreen from '../Parameterscreen';
var React = require('react');
var ReactDOM = require('react-dom');
var ReactBsTable = require('react-bootstrap-table');
var BootstrapTable = ReactBsTable.BootstrapTable;
var TableHeaderColumn = ReactBsTable.TableHeaderColumn

class MainMenu extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      items: [],
      subjects:[],
      links: [
        { id: "ParameterMenu", name: "Parameter Menu", componentName: ParameterMenu }
      ],
      authToken: null,
      auth: 'Basic ' + btoa('user' + ':' + '7b6Z3m6v9F1o0n9B6d6a'),
    };
  }

  componentDidMount() {
    fetch("http://localhost:8153/api.rsc/sampledb_research?$format=json", { headers: { authorization: this.state.auth } }) //'user:7b6Z3m6v9F1o0n9B6d6a'})
      .then(results => results.json())
      .then(d => {
        console.log(d.value)
        this.setState({
          items: d.value
        })
        })

  }

  render() {
    const options = {
      mode: "radio",
      hideSelectColumn: true,
      bgColor: 'gold',
      searchPosition: 'left',
      clickToSelect: true,
      onRowClick: function (row) {
        fetch(`http://localhost:8153/api.rsc/sampledb_subjects/`, { headers: { authorization: 'Basic ' + btoa('user' + ':' + '7b6Z3m6v9F1o0n9B6d6a') } }) //'user:7b6Z3m6v9F1o0n9B6d6a'})
          .then(response => response.json())
          .then(a => {
            console.log(a.value)
            this.setState({
              subjects: a.value
            })
          })

      }.bind(this)

    }
    return (
      <div>
        <div>
      <BootstrapTable
        data={this.state.items} options={options}  striped  hover condensed search>
        <TableHeaderColumn dataField="id" isKey width = '100' dataAlign="center">Id</TableHeaderColumn>
        <TableHeaderColumn dataField="city" width = '200' dataAlign="center">Name</TableHeaderColumn>
        <TableHeaderColumn dataField="description" width = '300' dataAlign="center">description</TableHeaderColumn>
      </BootstrapTable>
        </div>
        <Router>
          <ParameterScreen>
            <Switch>
              {this.state.links.map(({ name, id, componentName }) => (
                <Route path={`/${id}`} component={componentName} key={id} />
              ))}
              <div>
                <BootstrapTable
                  data={this.state.subjects} options={options} striped hover condensed search>
                  <TableHeaderColumn dataField="subject_id" isKey width='200' dataAlign="center">Subject ID</TableHeaderColumn>
                  <TableHeaderColumn dataField="id" width='200' dataAlign="center">ID</TableHeaderColumn>
                  <TableHeaderColumn dataField="name" width='300' dataAlign="center">Name</TableHeaderColumn>
                  <TableHeaderColumn dataField="description" width='300' dataAlign="center">description</TableHeaderColumn>

                </BootstrapTable>
              </div>
            </Switch>
          </ParameterScreen>
      </Router>
      </div>

    );
  }
}

export default MainMenu;
