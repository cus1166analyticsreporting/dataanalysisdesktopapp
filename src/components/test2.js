import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import {get_list_of_studies} from '../library/server_requests';
import {get_list_of_participants} from '../library/server_requests';
import {post_analysis_instance} from '../library/server_requests';
import {get_list_of_analysis_results} from '../library/server_requests';
import {get_analysis_result} from '../library/server_requests';
import {post_analysis_report} from '../library/server_requests';
import { connect } from 'react-redux';

class Test2 extends React.Component {
  render(){
    return (
      <div className = "mainmenu">
        <p>"I'm the test2 component."</p>
        <p>{this.state.title}</p>
        <p>{this.state.text}</p>
        <button onClick={this.test_get_list_of_studies}>List Studies</button>
        <button onClick={this.test_get_list_of_participants}>List Participants</button>
        {/* <button onClick={this.test_post_analysis_instance}>Post Analysis Instance</button>
        <button onClick={this.test_get_list_of_analysis_results}>List Of Analysis Results</button>
        <button onClick={this.test_get_analysis_result}>Get Analysis Instance</button>
        <button onClick={this.test_post_analysis_report}>Post Analysis Report</button> */}
      </div>
    )
  }

  constructor(props){
    super(props)
    this.state = {
      title: "",
      text:"Sending Post and Get Requests",
    }


    this.test_get_list_of_participants = this.test_get_list_of_participants.bind(this)
    this.test_get_list_of_studies = this.test_get_list_of_studies.bind(this)
    // this.test_get_list_of_analysis_results = this.test_get_list_of_analysis_results.bind(this)
    // this.test_get_analysis_result= this.test_get_analysis_result.bind(this)
    // this.test_post_analysis_report = this.test_post_analysis_report.bind(this)
    // this.test_post_analysis_instance = this.test_post_analysis_instance.bind(this)
  }


  test_get_list_of_studies(){
    const response_get_list_of_studies = (response) => this.setState({
      title: "Get Request for List of Studies",
      text: JSON.stringify(response)
    });
    get_list_of_studies(response_get_list_of_studies,this.props.idToken)
  }

  test_get_list_of_participants(){
    const response_get_list_of_participants = (response) => this.setState({
      title: "Get Request for List of Participants",
      text: JSON.stringify(response)
    });
    get_list_of_participants(response_get_list_of_participants, this.props.idToken, 1)
  }

  // test_get_list_of_analysis_results(){
  //   const runGR_LOAR = (response) => this.setState({
  //     title: "Get Request for List Of Analysis Results",
  //     text: JSON.stringify(response)
  //   });
  //   get_list_of_participants(runGR_LOAR, this.props.idToken)
  // }

  // // not going to use
  // test_get_analysis_result(){
  //   const runGR_AR = (response) => this.setState({
  //     title: "Get Request for Analysis Results for A Specific Study",
  //     text: JSON.stringify(response)
  //   });
  //   get_analysis_result(runGR_AR)
  // }


  // test_post_analysis_instance(){
  //   // var x = testPostRequest()
  //   const runPR =(response) => this.setState({
  //     title: "Post Request For Post Analysis Instance",
  //     text: JSON.stringify(response)
  //   });
  //   post_analysis_instance(runPR, this.props.idToken)
  // }

  // // not going to use
  // test_post_analysis_report(){
  //   const runPR_AR = (response) => this.setState({
  //     title: "Post Request for Analysis Report",
  //     text: JSON.stringify(response, this.props.idToken)
  //   });
  //   getanalysis_result(runPR_AR)
  // }


}

const mapStateToProps = state => {
  return {
    idToken: state.idToken.idToken,
  }
}

export default connect(mapStateToProps)(Test2);

