import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { setAuthToken, deleteAuthToken } from '../redux/actions';


class Testing extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      text: "I'm used for testing purposes. Don't mind me."
    }
    this.handleClick = this.handleClick.bind(this)
    this.displayState = this.displayState.bind(this)
    console.log(this.props.authToken)  // use for auth in server request 
  }

  handleClick(){
    this.setState({
      text: "clicked!"
    });
  }

  displayState(){
    this.props.changeAuthToken("My-New-Auth-Token.")
  }

  test_js(){
  }

  render(){
    return (
      <div className = "test">
        <button onClick={()=>this.displayState()}>Press Me!</button>
      </div>
    )
  }
}


// use for auth in server request
const mapStateToProps = state => {
  console.log("state:"+JSON.stringify(state))
  return {
    authToken: state.authToken.authToken
  }
}
 
const mapDispatchToProps = dispatch => {
  return {
    changeAuthToken: (newToken) => {
      dispatch(setAuthToken(newToken))
    }
  }
}

const Test = connect(
  mapStateToProps,
  mapDispatchToProps
)(Testing)

export default Test;
