// TODO: Separate container logic from component
// import Authentication from './authentication';
import React from 'react';
import ReactDOM from 'react-dom';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { setAuthToken,setIdToken, setRefreshToken } from '../redux/actions';
import jose from 'node-jose'

const CLIENT_ID = "5vnfpodeqg6vbl75gpr74lf0ct";
const USER_POOL_ID = 'us-west-2_HlKhuqK50'
const USERNAME='LilyZhang123*'
const PASSWORD='LilyZhang123*'

// cognito login
var cognito = require('amazon-cognito-identity-js');

class Login extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      password:PASSWORD,
      username:USERNAME
    }
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.login = this.login.bind(this);
    console.log(this.props.authToken)
  }

  handlePasswordChange(event) {
    this.setState({
      password:event.target.value
    });
    // console.log(this.state.password);
  }

  handleUsernameChange(event) {
    this.setState({
      username:event.target.value
    });
    // console.log(this.state.username);
  }

  login(e) {
    e.preventDefault()

    const { username, password } = this.state;

    // cognitoUser object can't access state for some reason
    const redirect = () => this.props.history.push('/PostLogin');
    const setAuthToken = newToken => this.props.setAuthToken(newToken);
    const setIdToken = newToken => this.props.setIdToken(newToken);
    const setRefreshToken = newToken => this.props.setRefreshToken(newToken);
    // redirect(); // comment this out to test login
    // return;

    var poolData = {
      UserPoolId : USER_POOL_ID,
      ClientId : CLIENT_ID};

    var userPool = new cognito.CognitoUserPool(poolData);

    var authenticationData = {
      Username : this.state.username,
      Password : this.state.password,
    };

    var authenticationDetails = new cognito.AuthenticationDetails(authenticationData);
    var userData = {
        Username : this.state.username,
        Pool : userPool
    };

    var cognitoUser = new cognito.CognitoUser(userData);
    console.log("Authenticating")

    cognitoUser.authenticateUser(authenticationDetails, {
        onSuccess: function (result) {
            console.log('access token + ' + result.getAccessToken().getJwtToken());
            console.log('id token: '+result.getIdToken().getJwtToken())
            try {
              setAuthToken(result.getAccessToken().getJwtToken());
              setRefreshToken(result.getIdToken().getJwtToken());
              setIdToken(result.getIdToken().getJwtToken());
              console.log("setAuthToken success!")
            } catch(e){
              console.log(e)
            }

            try {
              redirect();
              return;
            } catch (e) {
              console.log(e);
            }
        },

        onFailure: function(err) {
            console.log('error :(');
            console.log(err)
        },

        newPasswordRequired: function(userAttributes, requiredAttributes) {

              delete userAttributes.email_verified;
              console.log('userAttributes: ' + JSON.stringify(userAttributes));

              cognitoUser.completeNewPasswordChallenge(PASSWORD, userAttributes, {
                onSuccess: function (result) {
                  // callback.cognitoCallback("", userAttributes);
                  console.log("Successful new password challenge");
                  redirect();
                  return;
                },
                onFailure: function (err) {
                  // callback.cognitoCallback(err, null)
                  console.log(err);
                }
              });
          }
    });
  }

  render(){
    return (
      // <Authentication>
      <div className = "login">
        <p>"I'm the login component."</p>
        <form action="">
          <div>
              <label>Username:</label>
              <input type="text" value={this.state.username} onChange={this.handleUsernameChange}/>
          </div>
          <div>
              <label>Password:</label>
              <input type="text" value={this.state.password} onChange={this.handlePasswordChange}/>
          </div>
          <div>
              <button onClick={this.login}>Login</button>
          </div>
        </form>
      </div>
    // </Authentication>
    )
  }
}

// export default withRouter(Login);


const mapStateToProps = state => {
  return {
    authToken: state.authToken.authToken,
    idToken: state.idToken.idToken,
    refreshToken: state.refreshToken.refreshToken
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setAuthToken: (newToken) => {
      dispatch(setAuthToken(newToken))
    },
    setIdToken: (newToken)=> {
      dispatch(setIdToken(newToken))
    },
    setRefreshToken:(newToken)=> {
      dispatch(setRefreshToken(newToken))
    }
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Login));
