import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import { setAuthToken,setIdToken, setRefreshToken } from '../redux/actions';

class Authentication extends React.PureComponent{

    setAuthToken(newToken) {this.props.setAuthToken(newToken)};

    // setAuthToken(newToken){this.props.setAuthToken(newToken)}
    // setIdToken(newToken){this.props.setIdToken(newToken)}
    // setRefreshToken(newToken){this.props.setRefreshToken(newToken)}

    render(){
        return <div
            // setAuthToken = {setAuthToken}
            // setidtoken = {()=>{this.setIdToken(newToken)}}
            // setrefreshtoken = {()=>{this.setIdToken(newToken)}}
        >{React.cloneElement(this.props.children, {setAuthToken:this.setAuthToken.bind(this)})}</div>
    }
}

const mapStateToProps = state => {
  return {
    authToken: state.authToken.authToken,
    idToken: state.idToken.idToken,
    refreshToken: state.refreshToken.refreshToken
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setAuthToken: (newToken) => {
      dispatch(setAuthToken(newToken))
    },
    setIdToken: (newToken)=> {
      dispatch(setIdToken(newToken))
    },
    setRefreshToken:(newToken)=> {
      dispatch(setRefreshToken(newToken))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Authentication);

