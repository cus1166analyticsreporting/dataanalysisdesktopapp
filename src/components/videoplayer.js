var React = require('react');
var ReactDOM = require('react-dom');
class Player extends React.Component {
    render() {
        return (
            <div className='player-wrapper'>
                <ReactPlayer
                    url='https://www.youtube.com/watch?v=0CUuvFjicqk'
                    className='react-player'
                    playing
                    width='100%'
                    height='100%'
                />
            </div>
        )
    }
}
