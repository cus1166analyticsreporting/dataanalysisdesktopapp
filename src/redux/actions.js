// action types
export const SET_AUTH_TOKEN = 'SET_AUTH_TOKEN'
export const DELETE_AUTH_TOKEN = 'DELETE_AUTH_TOKEN'
export const SET_ID_TOKEN = 'SET_ID_TOKEN'
export const DELETE_ID_TOKEN = 'DELETE_ID_TOKEN'
export const SET_REFRESH_TOKEN = 'SET_REFRESH_TOKEN'
export const DELETE_REFRESH_TOKEN = 'DELETE_REFRESH_TOKEN'

// action creators
export function setAuthToken(newToken){
    return { type:SET_AUTH_TOKEN, newToken }
}
export function deleteAuthToken(){
    return { type:DELETE_AUTH_TOKEN }
}
export function setIdToken(newToken){
    return { type:SET_ID_TOKEN, newToken }
}
export function deleteIdToken(){
    return { type:DELETE_ID_TOKEN }
}
export function setRefreshToken(newToken){
    return { type:SET_REFRESH_TOKEN, newToken }
}
export function deleteRefreshToken(){
    return { type:DELETE_REFRESH_TOKEN }
}
