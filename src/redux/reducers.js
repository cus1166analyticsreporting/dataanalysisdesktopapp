import { combineReducers } from 'redux'
import {
  SET_AUTH_TOKEN,
  DELETE_AUTH_TOKEN,
  SET_ID_TOKEN,
  DELETE_ID_TOKEN,
  SET_REFRESH_TOKEN,
  DELETE_REFRESH_TOKEN
} from './actions'

function authToken(state = {authToken:'demo-default-authtoken'}, action) {
    switch(action.type){
        case SET_AUTH_TOKEN:
            return {authToken:action.newToken}
        case DELETE_AUTH_TOKEN:
            return {authToken:null}
        default:
            return state
    }
}

function idToken(state = {idToken:'demo-default-idtoken'}, action) {
    switch(action.type){
        case SET_ID_TOKEN:
            return {idToken:action.newToken}
        case DELETE_ID_TOKEN:
            return {idToken:null}
        default:
            return state
    }
}

function refreshToken(state={refreshToken:'demo-default-refreshToken'}, action) {
    switch(action.type){
        case SET_REFRESH_TOKEN:
        return {refreshToken:action.newToken}
        case DELETE_REFRESH_TOKEN:
        return {refreshToken:null}
        default:
        return state
    }
}

const dataAnalysisApp = combineReducers({
    authToken,
    idToken,
    refreshToken
})
   
export default dataAnalysisApp