import React, { Component } from 'react';
import ReactDOM from 'react-dom';
// import React Router
import {
  BrowserRouter as Router,
  Route,
  Link,
  Switch,
  withRouter,
  Redirect
} from 'react-router-dom';

// components
import Login from './components/login';
import Nav from './nav';
import MainScreen from './mainscreen';
import MainMenu from './components/mainmenu';
import Status from './components/status';
import ViewReports from './components/viewreports';
import Test from './components/test';
import Test2 from './components/test2';



// import ParameterMenu from './components/parametermenu';
// import TableData from "./components/singularselect";

class Container extends Component {
  constructor(props){
    super(props);
    this.state=({
      postLoginLinks:[
        {id:"MainMenu", name:"MAIN MENU", componentName:MainMenu},
        {id:"Status",name:"STATUS", componentName: Status},
        {id:"ViewReports",name:"VIEW REPORTS", componentName: ViewReports},
        {id:"Test", name:"TEST", componentName:Test},
        {id:"Test2", name: "Test2", componentName:Test2},
      ]
    });
  }

  postLogin(){
    // console.log("postLogin in container")
    this.props.history.push('/PostLogin')
  }

  render(){
    return (
      <Router>
        <Switch>
          <Route path={'/PostLogin'} render={(props) => (<PostLogin links={this.state.postLoginLinks} path={'/PostLogin'}/>)}/>
          <Route component={Login}/>
        </Switch>
      </Router>
    )
  }
}

// TODO: change this to nested
function PostLogin(props) {
  console.log(props.path);
  return (
    <div className={"container","section"}>

      <Nav>
        <div>
          <ul>
            {props.links.map(({ name, id }) => (
              <p className="big" key={id}>
                <Link to={`${props.path}/${id}`}>{name}</Link>
              </p>
            ))}
          </ul>
        </div>
      </Nav>

      <MainScreen>
        <Switch>
          {props.links.map(({ name, id, componentName }) => (
            <Route path={`${props.path}/${id}`} component={componentName} key={id}/>
          ))}
          <Route component={Test}/>
        </Switch>
      </MainScreen>
    </div>
  )
}

export default Container;
