import React from 'react';
import ReactDOM from 'react-dom';

class Nav extends React.Component {
  render(){
    return (
      <div className="nav">
        {this.props.children}
      </div>
    )
  }
}

export default Nav;
