import axios from 'axios';

const config = (idToken) => {
  return {
    headers: {
      'Content-type':'application/json',
      'Accept':'text/plain',
      'Authorization': idToken
    }
  }
}

//TODO: pass idToken into function
export function get_list_of_studies(onResponseFunction, idToken){

  axios.get('https://4s9ol9s034.execute-api.us-west-2.amazonaws.com/dev/listofstudies', config(idToken))
      .then(function (response) {
        console.log(response);
        console.log(JSON.stringify(config))
        //LS(JSON.stringify(response));
        return onResponseFunction(response)
      })
      .catch(function (error) {
        console.log(error);
      });
    }

 export function get_list_of_participants(onResponseFunction, idToken, study_id){

   axios.get('http://127.0.0.1:8080/studyparticipants/'+study_id
 )
      .then(function (response) {
        console.log(response);
        return onResponseFunction(response)
      })
      .catch(function (error) {
        console.log(error);
      })
}

export function get_list_of_analysis_results(onResponseFunction, idToken){

  axios.get('http://0.0.0.0:5000/listofanalysisresults')
     .then(function (response) {
       console.log(response);
       return onResponseFunction(response)
     })
     .catch(function (error) {
       console.log(error);
     })
}

export function get_analysis_result(onResponseFunction, idToken,analysis_result_id){

  axios.get('http://0.0.0.0:5000/analysisresult/'+analysis_result_id,
  {'Content-type':'application/json','Accept':'text/plain'},
  )
     .then(function (response) {
       console.log(response);
       return onResponseFunction(response)
     })
     .catch(function (error) {
       console.log(error);
     })
}

export function post_analysis_report(onResponseFunction, idToken, analysis_result_id, report){

  axios.post('http://0.0.0.0:5000/analysisreport', {"analysis_result_id" : analysis_result_id,
  "report" : report},
  {'Content-type':'application/json','Accept':'text/plain'},
  )
     .then(function (response) {
       console.log(response);
       return onResponseFunction(response)
     })
     .catch(function (error) {
       console.log(error);
     })
}

export function post_analysis_instance(onResponseFunction, idToken, study_id, participants, parameters){

  axios.post('http://0.0.0.0:5000/analysisinstance', { "study_id" : study_id,
  "participants" : participants, "parameters" : parameters},
  {'Content-type':'application/json','Accept':'text/plain'},
  )
  .then(function (response) {
    console.log(response);
    //PS(JSON.stringify(response));
    return onResponseFunction(response)
  })
  .catch(function (error) {
    console.log(error);
  });
}

export default {post_analysis_instance, get_list_of_studies, get_list_of_participants, get_list_of_analysis_results, get_analysis_result, post_analysis_report}
