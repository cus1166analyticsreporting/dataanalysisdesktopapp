__author__ = 'dkarchmer'
'''
This file demonstrates how to create a Django Rest Framework (DRF) APIView based API to provide
user access to AWS credentials via Cognito (In the script above, this is defined as http://127.0.0.1:8000/auth/aws)

NOTE: This is NOT intended to be fully working code. It may not, for example, contain all required imports
      It also does not explain how to hook this with Django and DRF. It assumes you know how to do so
      But the Class and associated functions are in fact fully functional.
'''
import logging
import boto3
import botocore

from flask import Flask, request

AWS_REGION = 'us-west-2'

# Get an instance of a logger
logger = logging.getLogger(__name__)

# Assumes a cognito identity pool has been created.
IDENTITY_POOL_ID = 'us-west-2_HlKhuqK50'
# Change based on what you entered on the AWS Cognito Dashboard (Custom tab)
DEVELOPER_PROVIDED_NAME = 'LilyZhang123*'
TOKEN_DURATION = 3400

def get_aws_open_id_token(username):
    client = boto3.client('cognito-identity', AWS_REGION)

    # This is what should be done by the Server (after proper login).
    # It requires the Role to have access to cognito
    # Given a valid user (per server's authentication), the API should return the Cognito Resp
    # directly to the client
    # See http://docs.aws.amazon.com/cognito/devguide/identity/concepts/authentication-flow/
    logger.info('Requesting open_id from Cognito for: {0}'.format(username))
    try:
        resp = client.get_open_id_token_for_developer_identity(
            IdentityPoolId=IDENTITY_POOL_ID,
            Logins={DEVELOPER_PROVIDED_NAME: username},
            TokenDuration=TOKEN_DURATION
        )
        logger.info("Identity ID: {0}".format(resp['IdentityId']))
        logger.info("Request ID : {0}".format((resp['ResponseMetadata']['RequestId'])))
    except botocore.exceptions.ClientError as e:
        logger.error(str(e))
        resp = None


    return resp


class APICognitoViewSet(APIView):
    """
    GET IdentityId/Token that can be used by client to get Temporary AWS credentials
    This is the class that services the /auth/aws GET API in the script above
    """

    def get(self, request, format=None):
        """
        Update IdentityID and Token from Cognito
        """
        if request.user.is_anonymous():
            # User most login before they can get a token
            # This not only ensures the user has registered, and has an account
            # but that the account is active
            return Response('User not recognized.', status=status.HTTP_403_FORBIDDEN)

        data_dic = {}

        resp = get_aws_open_id_token(request.user.username)
        if resp:
            mystatus = resp['ResponseMetadata']['HTTPStatusCode']
            data_dic['IdentityId'] = resp['IdentityId']
            data_dic['Token'] = resp['Token']
        else:
            logger.error('Something wrong with Cognito call')
            mystatus=status.HTTP_500_INTERNAL_SERVER_ERROR

        return Response(data_dic, status=mystatus)


urlpatterns = patterns(url(r'^auth/aws', APICognitoViewSet.as_view(), name='api-cognito'),

)