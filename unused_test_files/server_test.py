import requests
import json
import unittest
import random

class TestAPI(unittest.TestCase):

    # def test_getuserpool(self):
    #     print()
    #     print("Executing 'GET user pool test request' Test ")
    #     print("-----------------------------")
    #     ID_TOKEN ="eyJraWQiOiIxRHd3Q3Y4cjhnNGQ5NjI1dXZYWDVnakVwZzRtd01xaXV0eG5lS1RSVElNPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI1YzM0MWYzMi1hZjg1LTQwMGEtYTk4Zi00MjhjZDM1ZTBhN2EiLCJjb2duaXRvOmdyb3VwcyI6WyJkZW1vIl0sImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtd2VzdC0yLmFtYXpvbmF3cy5jb21cL3VzLXdlc3QtMl9IbEtodXFLNTAiLCJjb2duaXRvOnVzZXJuYW1lIjoiTGlseVpoYW5nMTIzKiIsImNvZ25pdG86cm9sZXMiOlsiYXJuOmF3czppYW06Ojc2MDk5NDc0MDA2ODpyb2xlXC9Db2duaXRvX0RlbW9JZGVudGl0eVBvb2xBdXRoX1JvbGUiXSwiYXVkIjoiNXZuZnBvZGVxZzZ2Ymw3NWdwcjc0bGYwY3QiLCJldmVudF9pZCI6IjE4MTZhMGU4LTQzNTQtMTFlOC1hZmM1LWE1MmY0OWFhZGViZiIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNTI0MDg4OTA5LCJleHAiOjE1MjQwOTI1MDksImlhdCI6MTUyNDA4ODkwOSwiZW1haWwiOiJ5aS56aGFuZzE1QHN0am9obnMuZWR1In0.pIQeYoFWj49nwEB0VkawhpeHd6_xppHAhdT2FNIdpXH_iQUnw-VEjExACo1mFEHq3g2y4uf7mic-M3OA4O_8yRKrZNBPtcVjuUWxtuahXr1HQfevdmou2Sa0eJVaUd25nreKiZ_i_mzDtoLxf616kk34AtjmTSJBUwK3NZQfFmv-0Nk0yDvmuhAopctYdc0MZhD6zrkmSsstegw0nkAQNrDMSOSSE1Atnx6nbxitiujrbxuv2RgYpaWZXNWYsGauRGp9dtPd6VPZ1oqfuaGMeQ_w728vkEk1yYq0XzaPxlK8GdfVpKLVbupW0GzfpTcTk_fx7FaiDj_G5y7ZvNZb9Q"
    #     url = "http://127.0.0.1:5000/test-token/"+ID_TOKEN
    #     r = requests.get(url)
    #     print("Response: "+r.text)
    #     self.assertIn("OK", r.text)



    # def test_postrequest(self):
    #     print()
    #     print("Executing 'POST request' Test ")
    #     print("-----------------------------")

    #     url = "http://127.0.0.1:5000/processstudy"
    #     headers = {'Content-type': 'application/json', 'Accept': 'text/plain' }
    #     data = {
    #          "studyid" : "studyid1",
    #          "participants" : ["participantid1" , "participantid2"],
    #          "parameters" : {"param1" : ["selection1", "selection2"]}
    #     }
    #     r = requests.post(url,data=json.dumps(data), headers=headers)
    #     print("Response: " + r.text)
    #     # Asset if response is right
    #     self.assertIn("OK",r.text)

    def test_getrequest(self):
        print()
        print("Executing 'GET listOfStudies request' Test ")
        print("-----------------------------")
        url = "http://127.0.0.1:5000/"
        r = requests.get(url)
        print("Response: " + r.text)
        # Asset if response is right
        self.assertIn("OK",r.text)


    # def test_getrequest_details(self):
    #     print()
    #     print("Executing 'GET request' Test ")
    #     print("-----------------------------")
    #     url = "http://127.0.0.1:5000/studyparticipants/study1id"
    #     r = requests.get(url)
    #     print("Response: " + r.text)
    #     # Asset if response is right
    #     self.assertIn("OK",r.text)
