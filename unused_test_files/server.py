from flask import Flask
import json
import requests
app = Flask(__name__)

def getListOfStudies():
    response_dict = {
        "status" : "OK",
        "data" : {"studies": \
            {"study1id" : {"name":"My First Study", "details":"details", "type":"type1"}, \
            "study2id" : {"name" : "My Second Study","details":"details","type":"type2"}} \
        }
    }
    return json.dumps(response_dict)

def getListOfParticipants():
    response_dict = {
        "status" : "OK",
        "data" : { "participants": ["p1","p2"]
        }
    }
    return json.dumps(response_dict)

def processStudy():
    response_dict = {
        "status" : "OK",
        "data" : {
             "studyid" : "studyid1",
             "participants" : ["participantid1" , "participantid2"],
             "parameters" : {"param1" : ["selection1", "selection2"]}
        }
    }
    return json.dumps(response_dict)

def authenticateRequest(token):
    auth_url = 'https://4s9ol9s034.execute-api.us-west-2.amazonaws.com/dev'
    auth_headers = {'content-type': 'application/json',
                'Authorization': token
                }
    r = requests.get(auth_url, headers=auth_headers)
    if "200" in str(r.status_code):
        return json.dumps({'authenticated':"OK",
        'response':str(r.text),
        'status':str(r.status_code),
        'headers':str(r.headers)})
    else:
        return json.dumps({'authenticated':"ERROR",
        'response':str(r.text),
        'status':str(r.status_code),
        'headers':str(r.headers)})


@app.route('/test-token/<string:token>', methods=['GET'])
def test_token_handler(token):
    if request.method == 'GET':
        return authenticateRequest(token)

@app.route('/listofstudies', methods=['GET'])
def all_studies_handler():
    return getListOfStudies()

@app.route('/studyparticipants/<string:study1id>',methods=['GET'])
def study_handler(study1id):
    return getListOfParticipants()

@app.route('/processstudy',methods=['POST'])
def study_processor():
    return processStudy()

@app.route('/',methods=['GET'])
def handle_get():
    if request.method == 'GET':
        return json.dumps({'text':'Welcome to the data analysis python server.'})

if __name__ == '__main__':
    app.debug = True
    app.run(host='0.0.0.0', port=5000)
