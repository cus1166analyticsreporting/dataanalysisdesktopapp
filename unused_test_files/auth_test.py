import requests
import json



print("Executing 'user pool request' Test ")
print("-----------------------------")

demo_url = 'https://bom5qbhtag.execute-api.us-west-2.amazonaws.com/dev/'
url = "https://4s9ol9s034.execute-api.us-west-2.amazonaws.com/dev/"
token = 'eyJraWQiOiIxRHd3Q3Y4cjhnNGQ5NjI1dXZYWDVnakVwZzRtd01xaXV0eG5lS1RSVElNPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI1YzM0MWYzMi1hZjg1LTQwMGEtYTk4Zi00MjhjZDM1ZTBhN2EiLCJjb2duaXRvOmdyb3VwcyI6WyJkZW1vIl0sImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtd2VzdC0yLmFtYXpvbmF3cy5jb21cL3VzLXdlc3QtMl9IbEtodXFLNTAiLCJjb2duaXRvOnVzZXJuYW1lIjoiTGlseVpoYW5nMTIzKiIsImNvZ25pdG86cm9sZXMiOlsiYXJuOmF3czppYW06Ojc2MDk5NDc0MDA2ODpyb2xlXC9Db2duaXRvX0RlbW9JZGVudGl0eVBvb2xBdXRoX1JvbGUiXSwiYXVkIjoiNXZuZnBvZGVxZzZ2Ymw3NWdwcjc0bGYwY3QiLCJldmVudF9pZCI6IjE4ZTQwNjExLTUyYzItMTFlOC04ODQ0LTlmODAyNDZkMWQyMCIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNTI1Nzg1NDIzLCJleHAiOjE1MjU3ODkwMjMsImlhdCI6MTUyNTc4NTQyMywiZW1haWwiOiJ5aS56aGFuZzE1QHN0am9obnMuZWR1In0.OW-T9EK5oUCdHO0uw2kIQxiVTe5YDt7qV37xOA2mP3FiiYShFe1TV0UaDAbCdDRVUdcS47YNC6z20G5QKGdQ0oe_FpMT1tbSpByiwXY2G162XlgcMiWbG_fOynHVAm6Cd46Hz9IEjZMJcGX5-L8M3b44_SR3frxAoC9aXZHIHrHegy0pqb0UlGZ-lsjmxRCHv4tMEbDMLr95rxQwffxpKhtCSUVAmpT12_wGjVfZdcfW-EW3nR2Xp31L3XGaCx5Upf91v5Jk_K02XHU9Fq0Apg7D8iikg5FOj6LGaSBVJ5ep6DYOD6vTnBnX5dvE-KfkP7tNxeSAwbro8cp6XGyKNg'
authorization_str = '%s' % token

headers = {'content-type': 'application/json',
        'Authorization': authorization_str}

r = requests.get(url, headers=headers)
print("Response: " + r.text)