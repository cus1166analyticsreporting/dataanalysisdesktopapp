__author__ = 'dkarchmer'
'''
This script emulates a stand-alone Python based client. It relies on Boto3 to access AWS, but
requires your Django server to have an API for your user to access Cognito based credentials

Because of Cognito, the client (this script) will only get temporary AWS credentials associated
to your user and only your user, and based on whatever you configure your AIM Policy to be.

Most Cognito examples demonstrate how to use Cognito for Mobile Apps, so this scripts demonstrate
how to create a stand-alone Python script but operating similarly to these apps.
'''
import json
import requests
import logging
import boto3
from boto3.session import Session

AWS_REGION = 'us-east-1'

AV_DOMAIN_NAME = 'https://demo622.auth.us-west-2.amazoncognito.com'
AV_API_PREFIX = ''
logger = logging.getLogger(__name__)

class Connection(object):

    token = 'eyJraWQiOiIxRHd3Q3Y4cjhnNGQ5NjI1dXZYWDVnakVwZzRtd01xaXV0eG5lS1RSVElNPSIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI1YzM0MWYzMi1hZjg1LTQwMGEtYTk4Zi00MjhjZDM1ZTBhN2EiLCJjb2duaXRvOmdyb3VwcyI6WyJkZW1vIl0sImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJpc3MiOiJodHRwczpcL1wvY29nbml0by1pZHAudXMtd2VzdC0yLmFtYXpvbmF3cy5jb21cL3VzLXdlc3QtMl9IbEtodXFLNTAiLCJjb2duaXRvOnVzZXJuYW1lIjoiTGlseVpoYW5nMTIzKiIsImNvZ25pdG86cm9sZXMiOlsiYXJuOmF3czppYW06Ojc2MDk5NDc0MDA2ODpyb2xlXC9Db2duaXRvX0RlbW9JZGVudGl0eVBvb2xBdXRoX1JvbGUiXSwiYXVkIjoiNXZuZnBvZGVxZzZ2Ymw3NWdwcjc0bGYwY3QiLCJldmVudF9pZCI6ImM3YWQ4Yzc3LTQyNzAtMTFlOC04YWIwLWI5YTM1MGIxY2ZjNSIsInRva2VuX3VzZSI6ImlkIiwiYXV0aF90aW1lIjoxNTIzOTkxMjc4LCJleHAiOjE1MjM5OTQ4NzgsImlhdCI6MTUyMzk5MTI3OSwiZW1haWwiOiJ5aS56aGFuZzE1QHN0am9obnMuZWR1In0.cbeEtUP3s81HQEV_RlcaGNNHLec6ZSymJNTGEJSQDi-VYVDgNLcSfhxFw-6zLDLzclm3rH3U7ISYFseyO1zlcTyCvBzpED7NQtYqPnDYheAvKLA1S1FKUz4iKmBHlvQLczkcfSXkh6SIePzP7EAYRg-K6RmsGEJYQeJXytcU4PrLApk4iuOTRGq9CiSdz8fapFHrW5Kql2T3OKvvA0TVV-FOzW7DULaSQP4mpyfSWk2H7L0nN_65Rgth2YQlHtZOiGSQA1JpYFdiDJqrc4WE2QfmY5KGONGNcHRrYWrmIjIvYL2LtyOflc9seNu-vrbzkV4fOmwveDNYMQkXuHdfNw'
    domain = AV_DOMAIN_NAME

    def __init__(self, domain=None):
        if domain:
            self.domain = domain

    def _get_url(self, api):
        url = '{0}/{1}/{2}'.format(self.domain, AV_API_PREFIX, api)
        logger.debug('Calling: ' + url)
        return url

    def _get_header(self, use_token):
        if use_token:
            if not self.token:
                raise('No Token')
            authorization_str = 'token %s' % self.token
            headers = {'content-type': 'application/json',
                       'Authorization': authorization_str}
        else:
            headers = {'Content-Type': 'application/json'}
        return headers

    def get(self, api, use_token):

        url = self._get_url(api)
        headers = self._get_header(use_token)

        r = requests.get(url, headers=headers)

        return r

     def do_login(self, api, username, password):

        url = self._get_url(api)
        headers = self._get_header(use_token=False)

        r = requests.post(url, auth=(username, password))

        return r

    # def login(self, password, email):
    #     data = { 'email': email, 'password': password }
    #     api = 'auth/login'

    #     r = self.post(api=api, data=data, use_token=False)
    #     if r.status_code == 200:
    #         content = json.loads(r.content.decode())
    #         self.token = content['token']
    #         self.username = content['username']
    #         logger.info('Welcome @{0} (token: {1})'.format(self.username, self.token))
    #         return True
    #     else:
    #         logger.error('Login failed: ' + str(r.status_code) + ' ' + r.content.decode())
    #         return False

    # def logout(self):
    #     api = 'auth/logout'
    #     r = self.post(api=api, use_token=True)
    #     if r.status_code == 204:
    #         logger.info('Goodbye @{0}'.format(self.username))
    #         self.username = None
    #         self.token = None
    #     else:
    #         logger.error('Logout failed: ' + str(r.status_code) + ' ' + r.content.decode())

if __name__ == '__main__':
    # Test
    # Logger Format
    from logging import StreamHandler, Formatter
    FORMAT = '[%(asctime)-15s] %(levelname)-6s %(message)s'
    DATE_FORMAT = '%d/%b/%Y %H:%M:%S'
    formatter = Formatter(fmt=FORMAT, datefmt=DATE_FORMAT)
    handler = StreamHandler()
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.DEBUG)

    c = Connection('http://127.0.0.1:8000')
    # c.login(email='user1@test.com', password='user1')
    r = c.get(api='auth/aws', use_token=True)
    logger.info(r.status_code)
    if r.status_code == 200:
        content = json.loads(r.content.decode())

        # Given the resp from the server, with an IdentityId and a Token, the client can directly
        # get credentials from Cognito, which should be based on a given IAM Role
        # (One that only allows access to wahtever services you want the client script to access. e.g. S3 uploads)
        client = boto3.client('cognito-identity', AWS_REGION)
        resp = client.get_credentials_for_identity(IdentityId=content['IdentityId'],
                                                   Logins={'cognito-identity.amazonaws.com': content['Token']})

        # The resp contains the actual temporary AWS secret/access codes and a session token, to be
        # used with the rest of the AWS APIs
        secretKey = resp['Credentials']['SecretKey']
        accessKey = resp['Credentials']['AccessKeyId']
        sessionToken = resp['Credentials']['SessionToken']

        # Now you can use Boto3 like you would if you were using your own secret keys
        # what you will see in any Boto3 example on the web
        session = Session(aws_access_key_id=accessKey,
                          aws_secret_access_key=secretKey,
                          aws_session_token=sessionToken,
                          region_name=AWS_REGION)

        s3_resource = session.resource('s3')
        bucket = s3_resource.Bucket('bucket-with-role-permision')
        print('\n' + str(bucket))
        for key in bucket.objects.all():
            logger.info(key.key)

    logger.info("-------------")

    c.logout()